module gitlab.com/signoz-public/spanprocessor

go 1.14

require (
	github.com/Shopify/sarama v1.27.2
	github.com/antonmedv/expr v1.8.9
	github.com/census-instrumentation/opencensus-proto v0.3.0
	github.com/gogo/protobuf v1.3.1
	github.com/golang/groupcache v0.0.0-20200121045136-8c9f03a8e57e
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/spf13/cast v1.3.1
	github.com/stretchr/testify v1.6.1
	go.opentelemetry.io/collector v0.14.0
	google.golang.org/grpc v1.33.2
	google.golang.org/protobuf v1.25.0
)
