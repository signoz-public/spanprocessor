package spanprocessor

import "gitlab.com/signoz-public/spanprocessor/consumer/pdata"

// Unmarshaller deserializes the message body.
type Unmarshaller interface {
	// Unmarshal deserializes the message body into traces.
	Unmarshal([]byte) (pdata.Traces, error)

	// Encoding of the serialized messages.
	Encoding() string
}

// defaultUnmarshallers returns map of supported encodings with Unmarshaller.
func DefaultUnmarshallers() map[string]Unmarshaller {
	otlp := &otlpProtoUnmarshaller{}
	// jaegerProto := jaegerProtoSpanUnmarshaller{}
	// jaegerJSON := jaegerJSONSpanUnmarshaller{}
	// zipkinProto := zipkinProtoSpanUnmarshaller{}
	// zipkinJSON := zipkinJSONSpanUnmarshaller{}
	// zipkinThrift := zipkinThriftSpanUnmarshaller{}
	return map[string]Unmarshaller{
		otlp.Encoding(): otlp,
		// jaegerProto.Encoding():  jaegerProto,
		// jaegerJSON.Encoding():   jaegerJSON,
		// zipkinProto.Encoding():  zipkinProto,
		// zipkinJSON.Encoding():   zipkinJSON,
		// zipkinThrift.Encoding(): zipkinThrift,
	}
}
